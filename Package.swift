// swift-tools-version:5.7
import PackageDescription
let package = Package(
 name: "IPSFramework",
 platforms: [.iOS(.v13)],
 products: [
    .library(name: "IPSFramework", targets: ["IPSFramework"]),
    .library(name: "IndoorMapSDK", targets: ["IndoorMapSDK"])
    ],
 dependencies: [],
 targets: [
     .binaryTarget(
         name: "IPSFramework",
         url: "https://cocoapods.oriient.me/IPSFramework/IPSFramework_8.2.2.zip",
         checksum: "ce5036e642c789f02a28c6c6be034ac5fa139912caeda28484e9a6df850ff5d5"
     ),
     .binaryTarget(
         name: "IndoorMapSDK",
         url: "https://cocoapods.oriient.me/IndoorMapSDK/IndoorMapSDK_8.2.2.zip",
         checksum: "3feac309a3a960887a486593a878a1b73792d41a4875d42799dc4fc803de371e"
     )
 ]
)
